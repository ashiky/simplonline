<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Simplonline</title>
</head>

<body>
    <main>
        <nav>
            <img src="images/logo-simplon-1.png" alt="logo-simplon">
            <div class="menuBurg">
            <div class="burger-icon"></div>
            <div class="burger-icon"></div>
            <div class="burger-icon"></div>
            </div>
            <a href="">Entreprises</a>
            <a href="">Réseaux</a>
            <a href="">A propos</a>
            <a href="">Collaborer</a>
            <a href="">Formations</a>
            <a href="">Contact</a>
        </nav> 
        <?php include "home.php" ?>
        <script src="script.js"></script>
</body>
</html>