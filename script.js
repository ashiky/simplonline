
const slidesContainer = document.getElementById("slides-container");
const slide = document.querySelector(".slide");
const prevButton = document.querySelectorAll(".slide-arrow-prev");
const nextButton = document.querySelectorAll(".slide-arrow-next");


nextButton.forEach(element => {
element.addEventListener("click", () => {
  const slideWidth = slide.clientWidth;
  slidesContainer.scrollLeft += slideWidth;
});
});
prevButton.forEach(element => {
element.addEventListener("click", () => {
  const slideWidth = slide.clientWidth;
  slidesContainer.scrollLeft -= slideWidth;
});
});