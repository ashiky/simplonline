    <header>
        <div class="container">
            <img src="images/logo-accueil.png" alt="logo-accueil">
            <div class="textHeader">
                <h1>Simplon,réseau de Fabrique numériques et inclusives</h1>
                <p>
                    Simplon.co est un réseau de Fabriques solidaires et inclusives qui
                    proposent des formations gratuitesaux métiers techniques du numérique
                    en France et à l’étranger
                </p>
            </div>
            <div class="bouton">
                <input class="RButton" type="button" value="Nos informations">
                <input class=" RButton WButton" type="button" value="Une question ?">
            </div>
        </div>
        <div class="imageAccueil">
            <img src="images/img-accueil.png" alt="img-accueil">
        </div>
    </header>
    <h2>Notre impact</h2>
    <div class="firstArt">
        <div class="carre">
            <div class="impact">
                <h3>8624</h3>
                <p>Simploniens dans le mondes</p>
                <div class="vector">
                    <img src="images/Vector.png" alt="vector">
                </div>
            </div>
            <div class="impact">
                <h3>99</h3>
                <p>Fabriques dans le monde</p>
                <div class="vector">
                    <img src="images/Vector.png" alt="vector">
                </div>
            </div>
            </div>
            <div class="carre">
            <div class="impact">
                <h3>70 %</h3>
                <p>De sorties positives après la formation</p>
                <div class="vector">
                    <img src="images/Vector.png" alt="vector">
                </div>
            </div>
            <div class="impact reverseImpact">
                <p>Notre bilan d’impact social</p>
                <div class="vector">
                    <img src="images/whiteVector.png" alt="vector">
                </div>
            </div>
        </div>
    </div>
    <h2>Témoignages</h2>
    
    <?php include"carrousel.php" ?>
    
    <h2>Apprentis Simplon</h2>
    <article class="apprentis">
        <div class="textApprent">
        <div class="imgMobile">
                <img src="images/img-apprentis.png" alt="">
            </div>
            <p class="titre">Les CFA Simplon proposent désormais des formations en apprentissage pour les jeunes</p>
            <p>Simplon propose depuis janvier toutes ses formations certifiantes au travers de titres professionnels en
                apprentissage</p>
            <input class="RButton" type="button" value="En savoir plus">
        </div>
    </article>
    <h2>Ils nous soutiennent</h2>
    <div class="soutien">
        <img src="images/logos-partenaires.png" alt="logo partenaires">
    </div>
    <footer>
        <div class="upFooter">
            <div>
                <img src="images/footer-logo-img.png" alt="logo simplon blanc">
            </div>
            <div class="centerFooter">
                <h4>Restez informés</h4>
                <p>Inscrivez-vous à la newsletter</p>
                <div class="mails">
                <input type="text" class="name">
                <img id="mail" src="images/mail.png" alt="mail">
                </div>
            </div>
        </div>
        <div class="reseaux">
            <a href=""><img src="images/fb.png"></a>
            <a href=""><img src="images/insta.png"></a>
            <a href=""><img src="images/twitter.png"></a>
            <a href=""><img src="images/youtube.png"></a>
            <a href=""><img src="images/linkedin.png"></a>
        </div>
        <div class="plus">
        <div class="imgMobile">
                <h4>Restez informés</h4>
                <p>Inscrivez-vous à la newsletter</p>
                <div class="mails">
                <input type="text" class="name">
                <img id="mail" src="images/mail.png" alt="mail">
                </div>
            </div>
            <a class="separ" href="https://example.com">Nos actulité</a>
            <a class="separ" href="https://example.com">Rejoindre nos équipes</a>
            <a class="separ" href="https://example.com">Notre agence numérique</a>
            <a class="separ" href="https://example.com">presse</a>
            <a class="separ" href="https://example.com">Contact</a>
            <a class="separ" href="https://example.com">Plan du site</a>
            <a class="separ" href="https://example.com">Mention légales</a>
            <a href="https://example.com">Accessibilité</a>
        </div>
    </footer>
</main>